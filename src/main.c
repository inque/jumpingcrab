#include "gameapp.h"
#include "utils.h"
#include "assets.h"
#include "raylib.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#ifdef __EMSCRIPTEN__
	#include <emscripten.h>
#elif _MSC_VER
	#ifdef NDEBUG
		//turn off console in Release mode in MSVC
		#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
	#endif
#endif

static void ParseArgs(int argc, char** argv){
	if(argc <= 1) return;
	for(int i = 1; i < argc; i++){
		char* arg = argv[i];
		arg += (*arg == '-');
		arg += (*arg == '-');
		switch(arg[0]){
			case 'c':
				i++;
				strncpy(app.configPath, argv[i], sizeof(app.configPath));
				break;
			case 'h':
				printf("Command line options: \n  --c [config path]\n\n");
				break;
		}
	}
}

static void InitializeApp(){
	LoadSettings();

	#ifndef DEDICATED_SERVER
	  	//regular client
		
		SetConfigFlags(FLAG_WINDOW_RESIZABLE);
		if(app.config.fullscreen) SetConfigFlags(FLAG_FULLSCREEN_MODE);
		if(app.config.vsync) SetConfigFlags(FLAG_VSYNC_HINT);
		InitWindow(app.config.resolution[0], app.config.resolution[1], GAME_TITLE);
		SetExitKey(0);
		SetWindowMinSize(640, 480);
		if((!app.config.fullscreen) && (app.config.windowPos[0] != -1)){
			SetWindowPosition(app.config.windowPos[0], app.config.windowPos[1]);
		}
		InitAudioDevice();
		
		#ifdef __EMSCRIPTEN__
			FixEmscriptenCanvasResizing();
			client.dpi = emscripten_get_device_pixel_ratio();
		#else
			client.dpi = 1;
		#endif

		LoadAssets();

		client.fsQuad = LoadRenderTexture(GetScreenWidth(), GetScreenHeight());
	#else
		//dedicated server

		LoadAssets();
		printf("Server is running on port potato.\n");
	#endif
	
	//uncomment to see the RAM usage
	/*
	#ifndef NDEBUG
		printf("\n\n game state: %lu kb. app state: %lu kb.", sizeof(game)/1024, sizeof(app)/1024);
		#ifndef DEDICATED_SERVER
			printf(" client state: %lu kb.", sizeof(client)/1024);
		#endif
		printf("\n\n");
	#endif
	*/
}

static void UpdateGame(){
	static unsigned int timeLast;
	unsigned int frameBudget;
	unsigned int timeNow;
	unsigned int timeDiff;
	
	DrawGraphics();

	frameBudget = (1000 / GET_GAME_TICKRATE());
	timeNow = Millisecs();
	timeDiff = timeNow - timeLast;
	while(timeDiff >= frameBudget){
		GameTick();
		timeDiff -= frameBudget;
		timeLast = timeNow;
		if(timeDiff > 200){
			break;
		}
	}
}

static void MainLoop(){
	#ifdef __EMSCRIPTEN__
		emscripten_set_main_loop(UpdateGame, 0, 0);
	#else
		#ifdef DEDICATED_SERVER
			while(true){
				UpdateGame();
				Delay(1);
			}
		#else
			while(!client.shouldClose){
				UpdateGame();
				if(!app.config.vsync){
					Delay(1);
				}
				if(WindowShouldClose()) break;
			}
			SaveSettings();
		#endif
	#endif
}

int main(int argc, char** argv){
	srand(time(NULL));
	ParseArgs(argc, argv);
	InitializeApp();
	MainLoop();
	return 0;
}
