#include "gameapp.h"

void InitializePhysics(){
    cpCollisionHandler* handler;

    cpSpaceInit(&game.space);
    cpSpaceSetGravity(&game.space, cpv(0, 9.12));

    handler = cpSpaceAddCollisionHandler(&game.space, COLLISION_TYPE_GUY, COLLISION_TYPE_LEVEL);
    handler->postSolveFunc = CallbackPostsolveGuy;
    
}

void UpdatePhysics(){
    for(int i=0; i < 1; i++){
        cpSpaceStep(&game.space, 0.12);
    }
}
