#include "gameapp.h"
#include "math.h"
#include "stdio.h"

int CreatePlayer(int isBot){
	for(int playerId=0; playerId < MAX_PLAYERS; playerId++){
		Player_t* player = GetPlayer(playerId);
		if(player->active) continue;

		player->active = 1;
		player->guyId = -1;
		player->isBot = isBot;
		player->score = 0;
		player->connectionId = -1;
		return playerId;
	}
	return -1;
}

void RemovePlayer(int id){
	Player_t* player = GetPlayer(id);
	player->active = 0;
}

static float coerce(float v, float min, float max){
	return fmaxf(fminf(v, max), min);
}

void UpdatePlayers(){
	forEachPlayer(player, playerId){
		if(player->guyId != -1){
			Guy_t* guy = GetGuy(player->guyId);
			
			guy->controls.buttonsLast = guy->controls.buttons;
			guy->controls.buttons = player->inputs.buttons;

			for(int i=0; i < 6; i++){
				player->inputs.axes[i] = coerce(player->inputs.axes[i], -1, 1);
			}

			// guy->controls.motorize[0] = -player->inputs.axes[0];
			// guy->controls.motorize[1] = -player->inputs.axes[1];
			// guy->controls.motorize[2] = player->inputs.axes[0];
			// guy->controls.motorize[3] = player->inputs.axes[1];

			for(int i=0; i < 5; i++){
				guy->controls.motorize[i] = player->inputs.axes[i];
				if((i==2)||(i==4)) guy->controls.motorize[i] *= -1;
				//printf("%f ", guy->controls.motorize[i]);
			}
			//printf("\n");
		}
	}
}
