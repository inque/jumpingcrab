#pragma once
#include "chipmunk/chipmunk.h"
#include "chipmunk/chipmunk_private.h"

enum{
    COLLISION_TYPE_LEVEL = 1,
    COLLISION_TYPE_GUY = 2
};

typedef enum{
    SHAPE_GUY,
    SHAPE_LV_PLATFORM,
    SHAPE_LV_PICKUP
} ShapeOwnerType_e;

typedef struct{
    ShapeOwnerType_e type;
    int id, sub_id;
} ShapeUserData_t;

void InitializePhysics();
void UpdatePhysics();
