#pragma once
#include "raymath.h"
#include "physics.h"
#include "transforms.h"

#define MAX_GUY_PARTS 8

typedef enum{
	GUY_DOG
} GuyType_e;

enum{
	BTN_SUICIDE = 1
};

typedef struct{
	float motorize[8];
	int buttons;
	int buttonsLast;
} GuyControls_t;

enum{
	DOG_PART_BODY,
	DOG_PART_UPPER_L_LEG,
	DOG_PART_UPPER_R_LEG,
	DOG_PART_LOWER_L_LEG,
	DOG_PART_LOWER_R_LEG,
	DOG_PART_L_FOOT,
	DOG_PART_R_FOOT,
	_DOG_PART_COUNT
};

typedef struct{
	int active;
	int health;
	cpBody* body;
	cpShape* shape;
	cpConstraint* joint;
	cpConstraint* motor;
	ShapeUserData_t shapeUserData;
	Transform_t tform;
} GuyPart_t;

typedef struct{
	int active;
	int playerId;
	GuyType_e type;
	GuyControls_t controls;
	GuyPart_t parts[MAX_GUY_PARTS];
	int fallingPlatformBonus;
	int isDead;
} Guy_t;

int SpawnGuy(GuyType_e type, int playerId, Vector2 pos);
void RemoveGuy(int id);
void DrawGuys();
void UpdateGuys();

void CallbackPostsolveGuy(cpArbiter* arb, cpSpace* space, void *data);
