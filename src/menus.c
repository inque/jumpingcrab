#include "gameapp.h"
#include "raylib.h"

#define RAYGUI_IMPLEMENTATION
#include "raygui.h"

#ifndef DEDICATED_SERVER

static void DrawBackground(){
	Color bgColor = {244,247,247,255};
	int stripeCount = 20;
	ClearBackground(bgColor);
	for(int i=0; i < stripeCount; i++){
		int stripeSize = client.height/stripeCount;
		Color stripeColor = {227,238,240,255};
		DrawRectangle(0, i*stripeSize*2, client.width, stripeSize, stripeColor);
	}
}

void MainMenu(){
	bool pressed;
	Rectangle btn = {client.halfWidth-150, client.halfHeight-32, 300, 64};
	
	DrawBackground();

	pressed = GuiButton(btn, "Start Game");
	if(pressed || IsKeyPressed(KEY_SPACE)){
		StartGame();
		client.screen = SCREEN_GAME;
	}

}

void PauseMenu(){
	bool pressed;
	Rectangle btn = {client.halfWidth-100, client.halfHeight-20, 200, 32};
	
	pressed = GuiButton(btn, "Continue");
	if(pressed || IsKeyPressed(KEY_ESCAPE)){
		client.paused = 0;
	}

	#ifndef __EMSCRIPTEN__
	btn.y += 40;
	pressed = GuiButton(btn, "Quit");
	if(pressed || IsKeyPressed(KEY_Q)){
		//CloseGame(true);
		//client.screen = SCREEN_MAINMENU;
		client.shouldClose = 1;
	}
	#endif

}

static const char* tooltipText = NULL;

static bool IsMouseInRect(Rectangle bounds){
	Vector2 mousePos = GetMousePosition();
	if(mousePos.x < bounds.x) return false;
	if(mousePos.y < bounds.y) return false;
	if(mousePos.x > (bounds.x + bounds.width)) return false;
	if(mousePos.y > (bounds.y + bounds.height)) return false;
	return true;
}

static void SetTooltip(Rectangle bounds, const char* text){
	if(!IsMouseInRect(bounds)) return;
	tooltipText = text;
}

static void DrawTooltip(){
	int fontSize = 20;
	if(tooltipText){
		Vector2 pos = GetMousePosition();
		Vector2 size = MeasureTextEx(GetFontDefault(), tooltipText, fontSize, 1.0);
		Rectangle bounds = {pos.x, pos.y, size.x+10, size.y+20};
		if((bounds.x + size.x) > (client.width - 80)) bounds.x -= (bounds.width + 30);
		if((bounds.y + size.y) > (client.height - 30)) bounds.y -= bounds.height;
		bounds.x += 30;

		DrawRectangleRec(bounds, (Color){0,0,0, 160});
		DrawText(tooltipText, bounds.x + 5, bounds.y + 5, fontSize, WHITE);

		tooltipText = NULL;
	}
}

static bool GuiButtonEx(Rectangle bounds, const char* text, const char* tooltip){
	SetTooltip(bounds, tooltip);
	return GuiButton(bounds, text);
}

void BottomBar(){
	int height = 30;
	int level = client.height-height;
	Rectangle bnd = {50, level+5, 80, 20};

	//DrawRectangle(0, level, client.width, height, Fade(LIGHTGRAY, 0.5));

	app.config.musicVolume = GuiSlider(bnd, "MUS", "", app.config.musicVolume, 0, 1);
	if(IsMouseInRect(bnd)) tooltipText = "Music volume";
	bnd.x += bnd.width + 40 + 5;

	app.config.soundVolume = GuiSlider(bnd, "SFX", "", app.config.soundVolume, 0, 1);
	if(IsMouseInRect(bnd)) tooltipText = "Audio volume";
	bnd.x += bnd.width + 40 + 5;

	app.config.motionBlur = GuiSlider(bnd, "BLR", "", app.config.motionBlur, 0, 1);
	if(IsMouseInRect(bnd)) tooltipText = "Motion blur";
	bnd.x += bnd.width + 40 + 5;

	bnd.width = 20;
	app.config.interpolate = GuiCheckBox(bnd, "INT", app.config.interpolate);
	if(IsMouseInRect(bnd)) tooltipText = "Frame interpolation";
	bnd.x += bnd.width + 40 + 5;

	bnd.width = 20;
	static bool slowMo;
	slowMo = GuiCheckBox(bnd, "SLW", slowMo);
	if(IsMouseInRect(bnd)) tooltipText = "Slooow mo";
	bnd.x += bnd.width + 40 + 5;
	app.config.tickrate = slowMo ? TICKS_PER_SEC/2 : TICKS_PER_SEC;

	DrawTooltip();
}

#endif