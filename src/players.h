#pragma once

typedef struct{
    float axes[8];
    int buttons;
} PlayerInputs;

typedef struct{
    int active;
    int guyId;
    int isBot;
    PlayerInputs inputs;
    int score;
    int connectionId;
} Player_t;

int CreatePlayer(int isBot);
void RemovePlayer(int id);
void UpdatePlayers();
