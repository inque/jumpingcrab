#include "gameapp.h"
#include "raylib.h"
#include "string.h"
#include "stdio.h"
#include "math.h"

int SpawnGuy(GuyType_e type, int playerId, Vector2 pos){
	cpSpace* space = &game.space;
	GuyPart_t* part;

	for(int guyId=0; guyId < MAX_GUYS; guyId++){
		Guy_t* guy = GetGuy(guyId);
		if(guy->active) continue;

		memset(guy, 0, sizeof(Guy_t));
		guy->active = 1;
		guy->isDead = 0;
		guy->playerId = playerId;
		guy->type = type;
		switch(type){
			case GUY_DOG: {
				float bodyWidth = app.secretOptions.bodyWidth, bodyHeight = 30;
				float legWidth = 10;
				float upperLegHeight = 25, lowerLegHeight = 35;
				float footWidth = 35, footHeight = 15;
				float mass, moment;
				
				mass = 2;
				moment = cpMomentForBox(mass, bodyWidth, bodyHeight);
				part = &guy->parts[DOG_PART_BODY];
				*part = (GuyPart_t){
					.active = 1,
					.health = 100,
					.body = cpSpaceAddBody(space, cpBodyNew(mass, moment)),
					.tform = {
						.size = {bodyWidth, bodyHeight}
					}
				};
				part->shape = cpSpaceAddShape(space, cpBoxShapeNew(part->body, bodyWidth, bodyHeight, 1));
				cpBodySetPosition(part->body, cpv(pos.x, pos.y));

				//upper legs L and R
				for(int i=0; i < 2; i++){
					float side = i*2-1;
					mass = 1;
					moment = cpMomentForBox(mass, legWidth, upperLegHeight);
					part = &guy->parts[DOG_PART_UPPER_L_LEG+i];
					*part = (GuyPart_t){
						.active = 1,
						.health = 100,
						.body = cpSpaceAddBody(space, cpBodyNew(mass, moment)),
						.tform = {
							.size = {legWidth, upperLegHeight}
						}
					};
					part->shape = cpSpaceAddShape(space, cpBoxShapeNew(part->body, legWidth, upperLegHeight, 1));
					cpVect bodyPos = cpv(pos.x + side*(bodyWidth/2+legWidth), pos.y + upperLegHeight/2);
					cpVect anchor = cpv(pos.x + side*(bodyWidth/2+legWidth/2), pos.y);
					cpBodySetPosition(part->body, bodyPos);
					part->joint = cpSpaceAddConstraint(space, cpPivotJointNew(guy->parts[DOG_PART_BODY].body, part->body, anchor));
					//part->motor = cpSpaceAddConstraint(space, cpSimpleMotorNew(part->body, guy->parts[DOG_PART_BODY].body, 0));
					//part->motor = cpSpaceAddConstraint(space, cpDampedRotarySpringNew(guy->parts[DOG_PART_BODY].body, part->body, side*3.14/2, 6.0e2, 15.0));
					part->motor = cpSpaceAddConstraint(space, cpDampedRotarySpringNew(guy->parts[DOG_PART_BODY].body, part->body, side*3.14/2, 6.0e2, 4.0));
					//part->motor = cpRotaryLimitJointNew(part->body, guy->parts[DOG_PART_BODY].body, -3.14, 3.14);
				}

				//lower legs L and R
				for(int i=0; i < 2; i++){
					float side = i*2-1;
					mass = 1;
					moment = cpMomentForBox(mass, legWidth, lowerLegHeight);
					part = &guy->parts[DOG_PART_LOWER_L_LEG+i];
					*part = (GuyPart_t){
						.active = 1,
						.health = 100,
						.body = cpSpaceAddBody(space, cpBodyNew(mass, moment)),
						.tform = {
							.size = {legWidth, lowerLegHeight}
						}
					};
					part->shape = cpSpaceAddShape(space, cpBoxShapeNew(part->body, legWidth, lowerLegHeight, 1));
					cpShapeSetFriction(part->shape, 0.95);
					cpVect bodyPos = cpv(pos.x + side*(bodyWidth/2+legWidth), pos.y + upperLegHeight/2 + lowerLegHeight);
					cpVect anchor = cpv(pos.x + side*(bodyWidth/2+legWidth/2), pos.y + upperLegHeight);
					cpBodySetPosition(part->body, bodyPos);
					
					part->joint = cpSpaceAddConstraint(space, cpPivotJointNew(guy->parts[DOG_PART_UPPER_L_LEG+i].body, part->body, anchor));
					
					part->motor = cpSpaceAddConstraint(space, cpDampedRotarySpringNew(guy->parts[DOG_PART_UPPER_L_LEG+i].body, part->body, side*-3.14/2, 6.0e2, 4.0));
					//part->motor = cpSpaceAddConstraint(space, cpDampedRotarySpringNew(guy->parts[DOG_PART_UPPER_L_LEG+i].body, part->body, side*3.14, 0.9, 0.95));
					//part->joint = cpSpaceAddConstraint(space, cpDampedSpringNew(guy->parts[DOG_PART_UPPER_L_LEG+i].body, part->body, anchor, anchor, 0.01, 0.9, 0.9));
					part->motor = cpSpaceAddConstraint(space, cpRotaryLimitJointNew(guy->parts[DOG_PART_UPPER_L_LEG+i].body, part->body, -0.5, 3.14));
					//part->motor = cpSpaceAddConstraint(space, cpGearJointNew(guy->parts[DOG_PART_UPPER_L_LEG+i].body, part->body, 0, 1));
				}

				//feet
				if(app.secretOptions.addFeet){
					for(int i=0; i < 2; i++){
						float side = i*2-1;
						mass = 1;
						moment = cpMomentForBox(mass, footWidth, footHeight);
						part = &guy->parts[DOG_PART_L_FOOT+i];
						*part = (GuyPart_t){
							.active = 1,
							.health = 100,
							.body = cpSpaceAddBody(space, cpBodyNew(mass, moment)),
							.tform = {
								.size = {footWidth, footHeight}
							}
						};
						part->shape = cpSpaceAddShape(space, cpBoxShapeNew(part->body, footWidth, footHeight, 1));
						cpShapeSetFriction(part->shape, 0.95);
						cpVect bodyPos = cpv(pos.x + side*(bodyWidth/2+legWidth), pos.y + upperLegHeight/2 + lowerLegHeight + footHeight);
						cpVect anchor = cpv(pos.x+ side*(bodyWidth/2+legWidth/2), pos.y + upperLegHeight + lowerLegHeight + footHeight);
						cpBodySetPosition(part->body, bodyPos);
						
						part->joint = cpSpaceAddConstraint(space, cpPivotJointNew(part->body, guy->parts[DOG_PART_LOWER_L_LEG+i].body, anchor));
						//part->motor = cpSpaceAddConstraint(space, cpPinJointNew(part->body, guy->parts[DOG_PART_LOWER_L_LEG+i].body, bodyPos, anchor));
						//part->motor = cpSpaceAddConstraint(space, cpRotaryLimitJointNew(part->body, guy->parts[DOG_PART_LOWER_L_LEG+i].body, -0.5*3.14, 0.5*3.14));
						part->motor = cpSpaceAddConstraint(space, cpDampedRotarySpringNew(part->body, guy->parts[DOG_PART_LOWER_L_LEG+i].body, side*-2*3.14, 6.0e2, 20));
					}
				}

				for(int i=0; i < _DOG_PART_COUNT; i++){
					part = &guy->parts[i];
					if(!part->active) continue;
					part->shapeUserData = (ShapeUserData_t){
						.type = SHAPE_GUY,
						.id = guyId,
						.sub_id = i
					};
					cpShapeSetUserData(part->shape, (cpDataPointer)&part->shapeUserData);
					cpShapeSetCollisionType(part->shape, COLLISION_TYPE_GUY);
				}

			} break;
		}
		return guyId;
	}
	return -1;
}

void RemoveGuy(int id){
	cpSpace* space = &game.space;
	Guy_t* guy = GetGuy(id);
	guy->active = 0;
	for(int i=0; i < MAX_GUY_PARTS; i++){
		GuyPart_t* part = &guy->parts[i];
		if(!part->active) continue;
		if(part->body) cpSpaceRemoveBody(space, part->body);
		if(part->shape) cpSpaceRemoveShape(space, part->shape);
		if(part->joint) cpSpaceRemoveConstraint(space, part->joint);
		if(part->motor) cpSpaceRemoveConstraint(space, part->motor);
		memset(part, 0, sizeof(GuyPart_t));
	}
}

void CallbackPostsolveGuy(cpArbiter* arb, cpSpace* space, void *data){
	CP_ARBITER_GET_SHAPES(arb, a, b);
	ShapeUserData_t* ourData = (ShapeUserData_t*)cpShapeGetUserData(a);
	ShapeUserData_t* otherData = (ShapeUserData_t*)cpShapeGetUserData(b);
	if(otherData != NULL){
		Guy_t* guy = GetGuy(ourData->id);
		GuyPart_t* part = &guy->parts[ourData->sub_id];
		cpFloat collisionImpulse = cpvlength(cpArbiterTotalImpulse(arb));
		if(part->health <= 0) return;
		// if((otherData->type == SHAPE_GUY) && (otherData->id == ourData->id)){
		// 	return;
		// }
		switch(otherData->type){
			case SHAPE_GUY:
				if(otherData->id == ourData->id){
					return;
				}
				break;
			case SHAPE_LV_PLATFORM:{
				LV_Platform_t* platform = &game.level.platforms[otherData->id];
				if(guy->type == GUY_DOG){
					if((ourData->sub_id == DOG_PART_LOWER_L_LEG) || (ourData->sub_id == DOG_PART_LOWER_R_LEG)){
						//cpBodySetVelocity(part->body, cpvmult(cpBodyGetVelocity(part->body), 1.3));
						//cpBodySetTorque(part->body, 1.75*cpBodyGetTorque(part->body));
						
						if(guy->parts[0].body){
							float jumping = -40; //-100;
							//cpBodyApplyForceAtWorldPoint(guy->parts[0].body, cpvmult(cpArbiterGetNormal(arb), jumping), cpArbiterGetPointA(arb, 0));
							//cpBodyApplyForceAtWorldPoint(part->body, cpvmult(cpArbiterGetNormal(arb), jumping), cpArbiterGetPointA(arb, 0));
							cpArbiterSetFriction(arb, 100000);

						}
					}
				}
				if(guy->fallingPlatformBonus && (!platform->body)){
					platform->toMakeFalling = 1;
				}
			} break;
			case SHAPE_LV_PICKUP: {
				LV_Pickup_t* pickup = &game.level.pickups[otherData->id];
				switch(pickup->type){
					case PICKUP_HEALTH:
						for(int otherPartId=0; otherPartId < MAX_GUY_PARTS; otherPartId++){
							GuyPart_t* otherPart = &guy->parts[otherPartId];
							if(otherPart->health <= 0) continue;
							otherPart->health += 50;
							if(otherPart->health > 100) otherPart->health = 100;
						}
						SpawnParticles(EFFECT_HEAL, 10, PeekPos(&part->tform), Vector2Zero());
						break;
					case PICKUP_FALLING_PLATFORMS:
						guy->fallingPlatformBonus += 15*TICKS_PER_SEC;
						break;
				}
				pickup->toBeRemoved = 1;
				//RemovePickup(otherData->id);
				break;
			} break;
		}
		if(collisionImpulse > 160){
			int damage = 10 + (int)(0.05 * collisionImpulse);
			cpVect point = cpArbiterGetPointA(arb, 0);
			cpVect normal = cpArbiterGetNormal(arb);
			part->health -= damage;
			SpawnParticles(EFFECT_BLOOD_SPLASH, 5+damage/5, Vec2(point), Vector2Scale(Vec2(normal), 1.25));
			//printf("boom damage!! %f. %i\n", collisionImpulse, part->health);
		}
		if(collisionImpulse > 70){
			if(ourData->sub_id == 0){
				#ifndef DEDICATED_SERVER
					if(guy->playerId == client.localPlayerId){
						SpawnParticles(EFFECT_DAMAGE_LOCAL, collisionImpulse/5, Vector2Zero(), Vector2Zero());
					}
				#endif
			}
		}
		if(collisionImpulse > 90){
			PlaySFX(SFX_FALL, PeekPos(&part->tform), 1.3*powf(1-collisionImpulse/250.0, 2), -1);
		}
	}
	return;
}

void DrawGuys(){
	#ifndef DEDICATED_SERVER
	GuyPart_t* part;
	Vector2 pos;
	float angle;

	forEachGuy(guy, guyId){
		switch(guy->type){
			case GUY_DOG: {

				for(int i=0; i < MAX_GUY_PARTS; i++){
					part = &guy->parts[i];
					if(part->active){
						if(!isInView(&part->tform)) continue;
						pos = GetPos(&part->tform);
						//DrawRectangle(pos.x, pos.y, 20, 20, RED);
						angle = GetAngle(&part->tform);
						Rectangle rec = {pos.x, pos.y, part->tform.size.x, part->tform.size.y};
						Vector2 origin = {part->tform.size.x/2, part->tform.size.y/2};
						Color bodyColor = (Color){245,60,50,255};
						if(guy->fallingPlatformBonus){
							if((game.frame>>3)&1){
								bodyColor = (Color){235,228,103,255};
							}
						}
						if(part->health < 100){
							DrawRectanglePro(rec, origin, angle, (Color){50,50,50,255} );
							if(i == DOG_PART_BODY){
								rec.width *= part->health/100.0;
							}else{
								rec.height *= part->health/100.0;
							}
						}
						DrawRectanglePro(rec, origin, angle, bodyColor);

						if(i == DOG_PART_BODY){
							int bleeding = (100-part->health);
							if(bleeding > 100) bleeding = 100;
							Vector2 offset = {-cosf(-angle*DEG2RAD), sinf(-angle*DEG2RAD)};
							Vector2 leftEye = {pos.x-offset.x*25, pos.y-offset.y*20};
							Vector2 rightEye = {pos.x+offset.x*25, pos.y+offset.y*20};
							Color eyeColor = {255,255-bleeding,255-bleeding,255};
							DrawCircle(leftEye.x, leftEye.y, 10, WHITE);
							DrawCircle(rightEye.x, rightEye.y, 10, WHITE);
							if(part->health > 0){
								DrawCircle(leftEye.x, leftEye.y, 4, BLACK);
								DrawCircle(rightEye.x, rightEye.y, 4, BLACK);
							}else{
								DrawLine(leftEye.x-5, leftEye.y-5, leftEye.x+5, leftEye.y+5, BLACK);
								DrawLine(leftEye.x+5, leftEye.y-5, leftEye.x-5, leftEye.y+5, BLACK);
								DrawLine(rightEye.x-5, rightEye.y-5, rightEye.x+5, rightEye.y+5, BLACK);
								DrawLine(rightEye.x+5, rightEye.y-5, rightEye.x-5, rightEye.y+5, BLACK);
							}
						}else{
							// if(pos.y < 4000){
							// 	char symbol[] = {0,0};
							// 	if(i == DOG_PART_UPPER_L_LEG){
							// 		symbol[0] = 'Q';
							// 	}else if(i == DOG_PART_LOWER_L_LEG){
							// 		symbol[0] = 'W';
							// 	}else if(i == DOG_PART_UPPER_R_LEG){
							// 		symbol[0] = 'O';
							// 	}else if(i == DOG_PART_LOWER_R_LEG){
							// 		symbol[0] = 'P';
							// 	}
							// 	DrawRectangle(pos.x-10, pos.y-10, 20, 20, (Color){255,255,255,100});
							// 	DrawText((char*)&symbol, pos.x-1, pos.y-1, 5, (Color){0,0,0,100});
							// 	DrawText((char*)&symbol, pos.x, pos.y, 5, (Color){64,128,128,255});
							// }
						}

					}
				}
			} break;
		}
	}
	
	#endif
}

void UpdateGuys(){
	forEachGuy(guy, guyId){
		
		if(guy->fallingPlatformBonus){
			guy->fallingPlatformBonus--;
		}

		for(int partId=0; partId < MAX_GUY_PARTS; partId++){
			GuyPart_t* part = &guy->parts[partId];
			if(!part->active) continue;

			UpdateTransform(&part->tform, part->body);
			if(guy->isDead) continue;

			if(part->health > 0){
				if(part->health > 100) part->health = 100;
				if(guy->type == GUY_DOG){
					float upperLegPower = 1000; //1500
					float lowerLegPower = 1700; //2000
					float tiltPower = 4500;
					switch(partId){
						case DOG_PART_BODY:
							//cpBodySetTorque(part->body, 0);
							//cpBodySetAngle(part->body, 0);
							//cpBodyApplyForceAtWorldPoint(part->body, cpv(10*guy->controls.motorize[0], 0), cpBodyGetPosition(part->body));
							//cpBodySetTorque(part->body, tiltPower*guy->controls.motorize[0]+cpBodyGetTorque(part->body));
							if(guy->controls.buttons & BTN_SUICIDE){
								part->health = 0;
							}

							break;
						case DOG_PART_UPPER_L_LEG:
							cpBodySetTorque(part->body, upperLegPower*guy->controls.motorize[1]+cpBodyGetTorque(part->body));
							break;
						case DOG_PART_UPPER_R_LEG:
							cpBodySetTorque(part->body, upperLegPower*guy->controls.motorize[2]+cpBodyGetTorque(part->body));
							break;
						case DOG_PART_LOWER_L_LEG:
							cpBodySetTorque(part->body, lowerLegPower*guy->controls.motorize[3]+cpBodyGetTorque(part->body));
							break;
						case DOG_PART_LOWER_R_LEG:
							cpBodySetTorque(part->body, lowerLegPower*guy->controls.motorize[4]+cpBodyGetTorque(part->body));
							break;
					}
				}
			}else{
				if(part->health < 0) part->health = 0;
				//the part is dead
				if(partId == 0){
					guy->isDead = 1;
					Player_t* player = GetPlayer(guy->playerId);
					player->guyId = -1;
					PlaySFX(SFX_LOSE, PeekPos(&part->tform), 1, guy->playerId);
					//guy->playerId = -1;
					//RemoveGuy(guyId);
				}else{
					//part->active = 0;
					if(part->joint){
						cpSpaceRemoveConstraint(&game.space, part->joint);
						part->joint = NULL;
					}
					if(part->motor){
						cpSpaceRemoveConstraint(&game.space, part->motor);
						part->motor = NULL;
					}
					if(guy->type == GUY_DOG){
						if(partId == DOG_PART_UPPER_L_LEG){
							guy->parts[DOG_PART_LOWER_L_LEG].health = 0;
						}
						if(partId == DOG_PART_UPPER_R_LEG){
							guy->parts[DOG_PART_LOWER_R_LEG].health = 0;
						}
					}
				}
				continue;
			}
			
			// if(partId == 0){
			// 	Vector2 pos = GetPos(&part->tform);
			// 	if( (pos.x < -game.level.sideLava) || (pos.x > game.level.sideLava) ){
			// 		Player_t* player = GetPlayer(guy->playerId);
			// 		player->guyId = -1;
			// 		part->health = 0;
			// 		//RemoveGuy(guyId);
			// 	}
			// }
		}
	}
}
