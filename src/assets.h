#pragma once
#include "raymath.h"

#ifndef DEDICATED_SERVER

#include "raylib.h"

#define MUS_BEATS 4

typedef enum{
    SFX_LOSE,
    SFX_FALL
} SfxType_e;

typedef struct{
    Sound musBeat[MUS_BEATS];
    Music melody1;
    Music bass1;
    Sound sfxLose;
    Sound sfxFall1;
} ClientAssets_t;

#endif

void LoadAssets();
void UpdateMusic();
void PlaySFX(SfxType_e sfx, Vector2 pos, float vol, int playerId);
