#pragma once
#include "stdint.h"

//random float from 0 to 1
#define randf() ((float)rand() / RAND_MAX)

//modulo operator, but no negative numbers
int mod2(int a, int b);

//current time in milliseconds
int Millisecs();

//sleep
void Delay(int ms);

typedef uint64_t FileModTime_t;
FileModTime_t FileModificationTime(const char* path);

#ifdef __EMSCRIPTEN__
    void FixEmscriptenCanvasResizing();
#endif
