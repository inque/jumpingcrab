#include "gameapp.h"
#include "gameplay.h"
#include "gameinterface.h"
#include "menus.h"
#include "raymath.h"
#include "stdbool.h"
#include "time.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#ifdef __EMSCRIPTEN__
	#include <emscripten.h>
#endif


AppState_t app = {
	.configPath = "options.txt",
	.secretOptions = {
		.bodyWidth = 100
	}
};
GameState_t game;

#ifndef DEDICATED_SERVER
	ClientState_t client = {0};
#endif

static void LocalPlayerControls(){
	#ifndef DEDICATED_SERVER
		Player_t* player = GetPlayer(client.localPlayerId);
		if(!player->active) return;

		// player->inputs.axes[0] = -IsKeyDown(KEY_Q) +IsKeyDown(KEY_W);
		// player->inputs.axes[1] = -IsKeyDown(KEY_O) +IsKeyDown(KEY_P);
		// player->inputs.axes[2] = -IsKeyDown(KEY_A) +IsKeyDown(KEY_S);
		// player->inputs.axes[3] = -IsKeyDown(KEY_L) +IsKeyDown(KEY_SEMICOLON);
		player->inputs.axes[0] = -IsKeyDown(KEY_Q) +IsKeyDown(KEY_E);
		player->inputs.axes[1] = -IsKeyDown(KEY_S) + (IsKeyDown(KEY_W) & (!IsKeyDown(KEY_D)));
		player->inputs.axes[2] = -IsKeyDown(KEY_S) + (IsKeyDown(KEY_W) & (!IsKeyDown(KEY_A)));
		player->inputs.axes[3] = IsKeyDown(KEY_A) * (IsKeyDown(KEY_S) ? -1 : 1);
		player->inputs.axes[4] = IsKeyDown(KEY_D) * (IsKeyDown(KEY_S) ? -1 : 1);
		
		player->inputs.buttons = 0;
		player->inputs.buttons = BTN_SUICIDE & IsKeyDown(KEY_K);
	#endif
}

void GameTick(){
	#ifndef DEDICATED_SERVER
		client.lastTickTime = GetTime();
		if(client.paused) return;
		UpdateMusic();
		if(client.screen != SCREEN_GAME) return;
	#endif

	if(app.shouldRestartRound){
		ResetGame();
		app.shouldRestartRound = 0;
	}
	UpdatePhysics();
	LocalPlayerControls();
	UpdatePlayers();
	UpdateGuys();
	UpdateLevel();
	UpdateGameplay();
	game.frame++;
}

void DrawGraphics(){
	#ifndef DEDICATED_SERVER
		client.width = GetScreenWidth();
		client.height = GetScreenHeight();
		client.halfWidth = client.width / 2;
		client.halfHeight = client.height / 2;
		client.cam.offset.x = (float)client.halfWidth;
		client.cam.offset.y = (float)client.halfHeight;
		client.scale = client.dpi;
		client.time = (float)GetTime();
		client.deltaTime = GetFrameTime();
		client.deltaFrame = ((client.time - client.lastTickTime) * (float)GET_GAME_TICKRATE());

		Vector2 topLeft = GetScreenToWorld2D(Vector2Zero(), client.cam);
		Vector2 bottomRight = GetScreenToWorld2D((Vector2){client.width, client.height}, client.cam);

		client.viewBounds = (Rectangle){topLeft.x, topLeft.y, bottomRight.x - topLeft.x, bottomRight.y - topLeft.y};

		if((client.fsQuad.texture.width != client.width) || (client.fsQuad.texture.height != client.height)){
			UnloadRenderTexture(client.fsQuad);
			client.fsQuad = LoadRenderTexture(client.width, client.height);
		}

		switch(client.screen){
			case SCREEN_MAINMENU: {
				#ifdef __EMSCRIPTEN__
					BeginDrawing();
						MainMenu();
					EndDrawing();
				#else
					StartGame();
					client.screen = SCREEN_GAME;
				#endif
			} break;
			case SCREEN_GAME: {
				BeginDrawing();
					#ifdef __EMSCRIPTEN__
						if(app.config.motionBlur>0){
							DrawRectangle(0, 0, client.width, client.height, Fade(WHITE, (app.config.motionBlur) ));
							DrawTextureRec(client.fsQuad.texture, (Rectangle){0,0,client.width,-client.height}, Vector2Zero(), WHITE);
							BeginTextureMode(client.fsQuad);
						}else{
							ClearBackground(RAYWHITE);
						}
					#else
						if(app.config.motionBlur>0){
							BeginTextureMode(client.fsQuad);
						}
						ClearBackground(RAYWHITE);
					#endif

					BeginMode2D(client.cam);
						DrawLevelBG();
					EndMode2D();
					DrawBackground();
					UpdateCam();
					BeginMode2D(client.cam);
						DrawParticles();
						DrawLevel();
						DrawGuys();
					EndMode2D();
					DrawUI();
					if(app.config.motionBlur>0){
						EndTextureMode();
						float opacity = (1.0 - app.config.motionBlur);
						#ifdef __EMSCRIPTEN__
							//opacity *= 0.5;
						#endif
						DrawTextureRec(client.fsQuad.texture, (Rectangle){0,0,client.width,-client.height}, Vector2Zero(), Fade(WHITE, opacity));
						//DrawTexture(client.fsQuad.texture, 0, 0, WHITE);
						//DrawTextureRec(client.fsQuad.texture, (Rectangle){0,client.height,client.width,-client.height}, Vector2Zero(), WHITE);
					}
				EndDrawing();
			} break;
		}
	#endif
}

void StartGame(){
	memset(&game, 0, sizeof(game));
	#ifndef DEDICATED_SERVER
		client.localPlayerId = CreatePlayer(0);
	#endif
	ResetGame();
}

void ResetGame(){
	game.state = STATE_UNINITIALIZED;
	game.frame = 0;
	game.timer = 0;

	if(!game.initialized){
		game.initialized = 1;
		InitializePhysics();
		#ifdef __EMSCRIPTEN__
			app.highScore = LoadStorageValue(0);
		#else
			app.highScore = app.config.highScore;
		#endif
	}else{
		CloseGame(false);
		app.gamesPlayed++;
		#ifdef __EMSCRIPTEN__
			SaveStorageValue(0, app.highScore);
		#endif
	}
	LoadLevel(0);

	#ifndef DEDICATED_SERVER
		client.cam = (Camera2D){
			.target = {0, 0},
			.rotation = 0,
			.zoom = 1
		};
	#endif
}

void CloseGame(bool full){
	forEachGuy(guy, guyId){
		RemoveGuy(guyId);
	}
	forEachPlayer(player, playerId){
		if(player->isBot || full){
			RemovePlayer(playerId);
		}else{
			player->score = 0;
		}
	}
	ClearLevel();
}
