#include "gameapp.h"
#include "string.h"
#include "stdlib.h"
#include "stdio.h"

void DefaultSettings(){
	app.config = (AppConfig_t){
		.fullscreen = 0,
		.vsync = 0,
		.resolution = {1024, 720},
		.windowPos = {-1, -1},
		.soundVolume = 0.725f,
		.musicVolume = 0.675f,
		.tickrate = TICKS_PER_SEC,
		.interpolate = 1,
		.port = 24659,
		.camWobbleAmplitude = 2.0,
		.camWobbleSpeed = 0.0075,
		.motionBlur = 0.8
	};
	#ifdef __EMSCRIPTEN__
		app.config.interpolate = 0;
		app.config.motionBlur = 0.45;
	#endif
}

static void* FindCfgKey(char* cfg, size_t len, const char* key){
	size_t keyLen = strlen(key);
	size_t p = 0;
	while(p < len){
		if(!strncmp(key, cfg, keyLen)){
			while(p < len){
				p++; cfg++;
				if(cfg[-1] == '=') break;
			}
			size_t valLen = 0;
			while((p + valLen) < len){
				valLen++;
				if(cfg[valLen] == 0x0A) break;
				if(cfg[valLen] == 0)    break;
			}
			cfg[valLen] = 0;
			return (void*)cfg;
		}
		while(p < len){
			p++; cfg++;
			if(cfg[-1] == 0x0A) break;
			if(cfg[-1] == 0)   break;
		}
	}
	return NULL;
}

static void GetCfgOption_i(char* cfg, int len, const char* key, int* out){
	void* val = FindCfgKey(cfg, len, key);
	if(val) *out = atoi(val);
}

static void GetCfgOption_f(char* cfg, int len, const char* key, float* out){
	void* val = FindCfgKey(cfg, len, key);
	if(val) *out = atof(val);
}

void LoadSettings(){
	DefaultSettings();
	
	FILE* file = fopen(app.configPath, "r");
	if(file){
		char cfg[8192];
		size_t len = fread(cfg, 1, sizeof(cfg)-1, file);
		cfg[len] = 0;
		fclose(file);

		GetCfgOption_i(cfg, len, "fullscreen",  &app.config.fullscreen);
		GetCfgOption_i(cfg, len, "vsync",       &app.config.vsync);
		GetCfgOption_i(cfg, len, "width",       &app.config.resolution[0]);
		GetCfgOption_i(cfg, len, "height",      &app.config.resolution[1]);
		GetCfgOption_i(cfg, len, "windowpos_x", &app.config.windowPos[0]);
		GetCfgOption_i(cfg, len, "windowpos_y", &app.config.windowPos[1]);
		int volume = 70;
		GetCfgOption_i(cfg, len, "sound_volume", &volume);
		app.config.soundVolume = volume/100.0f;
		GetCfgOption_i(cfg, len, "music_volume", &volume);
		app.config.musicVolume = volume/100.0f;
		GetCfgOption_i(cfg, len, "gamespeed",   &app.config.tickrate);
		GetCfgOption_i(cfg, len, "interpolation", &app.config.interpolate);
		GetCfgOption_i(cfg, len, "port", &app.config.port);
		GetCfgOption_f(cfg, len, "camwobbleamplitude", &app.config.camWobbleAmplitude);
		GetCfgOption_f(cfg, len, "camwobblespeed", &app.config.camWobbleSpeed);
		GetCfgOption_f(cfg, len, "motionblur", &app.config.motionBlur);
		GetCfgOption_i(cfg, len, "highscore", &app.config.highScore);
	}
}

static void WriteCfgOption_i(const char* key, int value, FILE* file){
	char line[1024];
	int len = sprintf(line, "%s=%i\n", key, value);
	fwrite(line, 1, len, file);
}

static void WriteCfgOption_f(const char* key, float value, FILE* file){
	char line[1024];
	int len = sprintf(line, "%s=%f\n", key, value);
	fwrite(line, 1, len, file);
}

void SaveSettings(){
	FILE* file = fopen(app.configPath, "w");
	#ifndef DEDICATED_SERVER
		if(!app.config.fullscreen){
			Vector2 windowPos = GetWindowPosition();
			app.config.windowPos[0] = (int)windowPos.x;
			app.config.windowPos[1] = (int)windowPos.y;
			app.config.resolution[0] = GetScreenWidth();
			app.config.resolution[1] = GetScreenHeight();
		}
	#endif
	if(file){
		WriteCfgOption_i("fullscreen",  app.config.fullscreen,    file);
		WriteCfgOption_i("vsync",       app.config.vsync,         file);
		WriteCfgOption_i("width",       app.config.resolution[0], file);
		WriteCfgOption_i("height",      app.config.resolution[1], file);
		WriteCfgOption_i("windowpos_x", app.config.windowPos[0],  file);
		WriteCfgOption_i("windowpos_y", app.config.windowPos[1],  file);
		WriteCfgOption_i("sound_volume", (int)(app.config.soundVolume*100), file);
		WriteCfgOption_i("music_volume", (int)(app.config.musicVolume*100), file);
		WriteCfgOption_i("gamespeed",   app.config.tickrate, file);
		WriteCfgOption_i("interpolation", app.config.interpolate, file);
		WriteCfgOption_i("port",          app.config.port, file);
		WriteCfgOption_f("camwobbleamplitude", app.config.camWobbleAmplitude, file);
		WriteCfgOption_f("camwobblespeed", app.config.camWobbleSpeed, file);
		WriteCfgOption_f("motionblur", app.config.motionBlur, file);
		WriteCfgOption_i("highscore",  app.config.highScore, file);
		fclose(file);
	}
}
