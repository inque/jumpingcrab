#include "gameapp.h"
#include "menus.h"
#include "stdio.h"
#include "math.h"

void UpdateCam(){
	#ifndef DEDICATED_SERVER
		Player_t* player = GetPlayer(client.localPlayerId);
		if(player->guyId != -1){
			Guy_t* guy = GetGuy(player->guyId);
			Vector2 campos = client.cam.target;
			Vector2 target = (guy->parts[0].active) ? GetPos(&guy->parts[0].tform) : campos;
			target.y += client.height / 4;
			float dist = Vector2Distance(target, client.cam.target);
			float threshold = (client.width+client.height)/16;
			
			if(dist > threshold){
				float dist2 = (dist - threshold);
				float speed = fminf(0.1*dist2*dist2*client.deltaTime, 1);
				campos = Vector2Lerp(campos, target, speed);
			}
			client.cam.target = campos;

			client.cam.rotation = app.config.camWobbleAmplitude*sinf(app.config.camWobbleSpeed*client.camWobblePhase);
			//client.cam.rotation += 1.2*cosf(0.0022*client.camWobblePhase);
			client.camWobblePhase += (0.2 + client.camWobblePower) * client.deltaFrame;
			client.camWobblePower -= 0.01 * client.deltaFrame;
			if(client.camWobblePower < 0) client.camWobblePower = 0;
		}

	#endif
}

void DrawUI(){
	char text[2048];
	Player_t* player = GetPlayer(client.localPlayerId);

	if(client.paused){
		PauseMenu();
		BottomBar();
		return;
	}else{
		if(IsKeyPressed(KEY_ESCAPE)){
			client.paused = 1;
		}
	}

	if(player->guyId == -1){
		sprintf(text, "Game over!\nYour score: %i.", player->score);
		int fontSize = 40;
		int textOffset = -MeasureText(text, fontSize)/2;
		Color textColor = {24,109,252,255};
		DrawText(text, client.halfWidth+textOffset-2, client.halfHeight-100-2, fontSize, BLACK);
		DrawText(text, client.halfWidth+textOffset, client.halfHeight-100, fontSize, textColor);
		if(player->score == app.highScore){
			if(app.gamesPlayed > 1){
				Color fancyColor = { (char)(128+128*sinf(0.5*client.time)), (char)(128+128*cosf(10+0.9*client.time)), (char)(128+128*sinf(-0.2*client.time), 255)};
				DrawText("A new high score!", client.halfWidth-150, client.halfHeight+120, 30, fancyColor);
			}
		}else{
			sprintf(text, "High score: %i", app.highScore);
			DrawText(text, client.halfWidth-150, client.halfHeight+120, 25, textColor);
		}
		DrawText("Press SPACE to restart", client.halfWidth-100, client.halfHeight+165, 20, textColor);
	}

	if(game.state <= STATE_STARTING){
		float opacity = 1-fminf((float)game.timer/(2*TICKS_PER_SEC), 1.0);
		DrawRectangle(0, 0, client.width, client.height, (Color){0,0,0,(char)(opacity*255)});
	}

	if(IsKeyPressed(KEY_SPACE)){
		app.shouldRestartRound = 1;
	}
	// if((game.state == STATE_ENDED) && IsKeyPressed(KEY_SPACE)){
	// 	app.shouldRestartRound = 1;
	// }

	BottomBar();
}

static Color MixColors4(Color c1, Color c2, Color c3, Color c4, float m1, float m2, float m3, float m4){
	return (Color){
		(char)(( (c1.r/255.0*m1)+(c2.r/255.0*m2)+(c3.r/255.0*m3)+(c4.r/255.0*m4) )/4*255),
		(char)(( (c1.g/255.0*m1)+(c2.g/255.0*m2)+(c3.g/255.0*m3)+(c4.g/255.0*m4) )/4*255),
		(char)(( (c1.b/255.0*m1)+(c2.b/255.0*m2)+(c3.b/255.0*m3)+(c4.b/255.0*m4) )/4*255),
		(char)(( (c1.a/255.0*m1)+(c2.a/255.0*m2)+(c3.a/255.0*m3)+(c4.a/255.0*m4) )/4*255)
	};
}

void DrawBackground(){
	char text[2048];
	Player_t* player = GetPlayer(client.localPlayerId);

	Color bgBase = {38,53,62,150};
	Color bg1 = {bgBase.r+24*sinf(4.2*client.time),2*bgBase.g,bgBase.b,bgBase.a+30*sinf(1.011*client.time)};
	Color bg2 = {bgBase.r*2+18*cosf(2.075*client.time),bgBase.g+35*sinf(3.04*client.time),bgBase.b,bgBase.a};
	Color bg3 = {bgBase.r*3,bgBase.b,bgBase.g+15*cosf(1.3*client.time),2*bgBase.a};
	Color bg4 = {bgBase.g,bgBase.g+13*sinf(0.22*client.time),2*bgBase.b,bgBase.a};
	float phase = sinf(0.25*client.time);
	float mix1 = fabsf(phase);
	float mix2 = fabsf(phase-0.25);
	float mix3 = fabsf(phase-0.5);
	float mix4 = fabsf(phase-0.75);
	//DrawRectangleGradientEx((Rectangle){0,0,client.width,client.height}, bg1, bg2, bg3, bg4);
	DrawRectangleGradientEx(
		(Rectangle){0,0,client.width,client.height},
		MixColors4(bg1,bg2,bg3,bg4, mix1,mix2,mix3,mix4),
		MixColors4(bg1,bg2,bg3,bg4, mix2,mix3,mix4,mix1),
		MixColors4(bg1,bg2,bg3,bg4, mix3,mix4,mix1,mix2),
		MixColors4(bg1,bg2,bg3,bg4, mix4,mix1,mix2,mix3)
	);

	if(player->guyId != -1){
		float textPhase = sinf(0.5*client.time);
		Color textColor = {24,109,252,200};
		sprintf(text, "Score: %i", player->score);
		DrawText(text, client.width/4, 40 +30*textPhase, 100, textColor);
		if(app.gamesPlayed > 0){
			sprintf(text, "High score: %i", app.highScore);
			DrawText(text, client.width/4+150, 150 +35*textPhase, 25, Fade(textColor, 0.5));
		}
	}
}
