#include "gameapp.h"
#include "math.h"

#define BOILERPLATE() int a = game.frame & 1; int b = 1-a;

void PinTransform(Transform_t* tform, Vector2 pos, float angle, Vector2 size){
	for(int i=0; i < 2; i++){
		tform->pos[i] = pos;
		tform->angle[i] = angle;
	}
	tform->size = size;
}

void UpdateTransform(Transform_t* tform, cpBody* body){
	BOILERPLATE();
	cpVect pos = cpBodyGetPosition(body);
	tform->pos[a] = (Vector2){pos.x, pos.y};
	tform->angle[a] = cpBodyGetAngle(body);
}

Vector2 PeekPos(Transform_t* tform){
	BOILERPLATE();
	return tform->pos[a];
}

Vector2 GetPos(Transform_t* tform){
	BOILERPLATE();
	#ifndef DEDICATED_SERVER
		if(app.config.interpolate){
			return Vector2Lerp(tform->pos[a], tform->pos[b], client.deltaFrame);
		}else{
			return tform->pos[a];
		}
	#else
		return Vector2Zero();
	#endif
}

float PeekAngle(Transform_t* tform){
	BOILERPLATE();
	return tform->angle[a];
}

float GetAngle(Transform_t* tform){
	BOILERPLATE();
	#ifndef DEDICATED_SERVER
		float res;
		if(app.config.interpolate){
			res = tform->angle[a] + client.deltaFrame * (tform->angle[1] - tform->angle[0]);
		}else{
			res = tform->angle[a];
		}
		return res * RAD2DEG;
	#else
		return 0;
	#endif
}

Rectangle GetBounds(Transform_t* tform){
	BOILERPLATE();
	return (Rectangle){
		tform->pos[a].x - tform->size.x / 2,
		tform->pos[a].y - tform->size.y / 2,
		tform->size.x,
		tform->size.y
	};
	//todo: angle
}

bool DoesIntersectRect(Transform_t* tform, Rectangle rec){
	BOILERPLATE();
	return CheckCollisionRecs(GetBounds(tform), rec);
}

bool isInView(Transform_t* tform){
	#ifdef DEDICATED_SERVER
		return true;
	#else
		return DoesIntersectRect(tform, client.viewBounds);
	#endif
}

