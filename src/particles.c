#include "gameapp.h"
#include "utils.h"
#include "math.h"
#include "stdlib.h"
#include "stdio.h"

#ifndef DEDICATED_SERVER

static Particle_t* AddParticle(ParticleSystem_t* fx, ParticleType_e type, Vector2 pos, float life, float randomPosRadius, Vector2 minVelocity, Vector2 maxVelocity){
	for(int partId = fx->nextFreeParticle; partId < (MAX_PARTICLES-1); partId++){
		Particle_t* part = &fx->particles[partId];
		if(part->type != PARTICLE_NONE) continue;

		part->type = type;
		part->pos = pos;
		{
			float a = 2*3.14*randf();
			float d = randomPosRadius*randf();
			part->pos.x += d*sinf(a);
			part->pos.y += d*cosf(a);
		}
		part->velocity.x = minVelocity.x + (maxVelocity.x - minVelocity.x) * randf();
		part->velocity.y = minVelocity.y + (maxVelocity.y - minVelocity.y) * randf();
		part->life = life;

		fx->nextFreeParticle = partId+1;
		if(fx->nextFreeParticle >= MAX_PARTICLES){
			fx->nextFreeParticle = 0;
		}
		if(partId >= fx->particleCount){
			fx->particleCount = partId+1;
		}
		return part;
	}
	//return NULL;
	return &fx->particles[0];
}

static void RemoveParticle(ParticleSystem_t* fx, int id){
	fx->particles[id].type = PARTICLE_NONE;
	if(id < fx->nextFreeParticle){
		fx->nextFreeParticle = id;
	}
	if(id == (fx->particleCount-1)){
		fx->particleCount--;
	}
}

#endif

void SpawnParticles(ParticleEffect_e effect, int amount, Vector2 pos, Vector2 velocity){
	#ifndef DEDICATED_SERVER
		ParticleSystem_t* fx = &client.fx;
		switch(effect){
			case EFFECT_HEAL: {
				for(int i=0; i < amount; i++){
					AddParticle(fx, PARTICLE_BONUS, pos, 100, 50, (Vector2){-0.02, -0.04}, (Vector2){0.02, -0.01});
				}
			} break;
			case EFFECT_BLOOD_SPLASH: {
				Vector2 minVel = Vector2Add(velocity, (Vector2){-0.2, -0.6});
				Vector2 maxVel = Vector2Add(velocity, (Vector2){0.2, -0.15});
				if(amount > 40) amount = 40;
				if(velocity.y > 0) velocity.y *= -1;
				for(int i=0; i < amount; i++){
					AddParticle(fx, PARTICLE_BLOOD, pos, 250+(40*(i&3)), 40, minVel, maxVel);
				}
			} break;
			case EFFECT_DAMAGE_LOCAL: {
				if(amount > 100) amount = 100;
				if(amount < 0) amount = 0;
				client.camWobblePhase += 5*amount;
			} break;
		}
	#endif
}

void DrawParticles(){
	#ifndef DEDICATED_SERVER
		ParticleSystem_t* fx = &client.fx;

		for(int partId=0; partId < fx->particleCount; partId++){
			Particle_t* part = &fx->particles[partId];
			if(part->type == PARTICLE_NONE) continue;
			Vector2 pos = Vector2Add(part->pos, Vector2Scale(part->velocity, client.deltaFrame));

			switch(part->type){
				case PARTICLE_BONUS: {
					float opacity = part->life/100.0;
					float size = 10+(6+2*(partId&3))*0.3*(100-part->life);
					Color color = {255,230,130,255};
					DrawRectangle(pos.x-size/2, pos.y-size/2, size, size, Fade(color, opacity*opacity));
				} break;
				case PARTICLE_BLOOD: {
					Vector2 endPoint = Vector2Subtract(pos, Vector2Scale(part->velocity, 6.0f));
					DrawLineEx(pos, endPoint, 8.0f, (Color){247, 40, 43, 190});
					//part->velocity.x *= 0.97f;
					part->velocity.y += 0.005f * client.deltaFrame;
				} break;
			}
			
			part->pos = Vector2Add(part->pos, part->velocity);
			part->life -= client.deltaFrame;
			if(part->life <= 0){
				RemoveParticle(fx, partId);
			}
		}
	#endif
}
