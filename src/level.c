#include "gameapp.h"
#include "stdio.h"
#include "stdlib.h"

static void RemovePlatformData(int id){
	Level_t* lv = &game.level;
	LV_Platform_t* platform = &lv->platforms[id];
	if(platform->body){
		cpSpaceRemoveBody(&game.space, platform->body);
		platform->body = NULL;
	}
	if(platform->shape){
		cpSpaceRemoveShape(&game.space, platform->shape);
		platform->shape = NULL;
	}
}

void AddPlatform(Rectangle bounds){
	Level_t* lv = &game.level;
	LV_Platform_t* platform = NULL;
	int platformId = -1;
	if(lv->platformCount >= LV_MAX_PLATFORMS) return;
	if(lv->recycleLine > 0){
		for(int i=0; i < lv->platformCount; i++){
			LV_Platform_t* otherPlatform = &lv->platforms[i];
			if(otherPlatform->bounds.y < lv->recycleLine){
				RemovePlatformData(i);
				platform = otherPlatform;
				platformId = i;
				break;
			}
		}
	}
	if(platform == NULL){
		platformId = lv->platformCount++;
		platform = &lv->platforms[platformId];
	}
	platform->bounds = bounds;
	cpVect verts[] = {
		{bounds.x, bounds.y},
		{bounds.x + bounds.width, bounds.y},
		{bounds.x + bounds.width, bounds.y + bounds.height},
		{bounds.x, bounds.y + bounds.height}
	};
	platform->shape = cpSpaceAddShape(&game.space, cpPolyShapeNew(cpSpaceGetStaticBody(&game.space), 4, (cpVect*)&verts, cpTransformIdentity, 1));
	platform->shapeUserData = (ShapeUserData_t){
		.type = SHAPE_LV_PLATFORM,
		.id = platformId
	};
	cpShapeSetFriction(platform->shape, 0.9);
	cpShapeSetUserData(platform->shape, (cpDataPointer)&platform->shapeUserData);
	cpShapeSetCollisionType(platform->shape, COLLISION_TYPE_LEVEL);
	platform->body = NULL;
	PinTransform(&platform->tform, (Vector2){bounds.x+bounds.width/2, bounds.y+bounds.height/2}, 0, (Vector2){bounds.width, bounds.height});
	platform->toMakeFalling = 0;
}

void MakePlatformFall(int id){
	Level_t* lv = &game.level;
	LV_Platform_t* platform = &lv->platforms[id];

	if(platform->body) return;
	RemovePlatformData(id);

	Vector2 size = {platform->bounds.width, platform->bounds.height};
	float mass = 10;
	float moment = cpMomentForBox(mass, size.x, size.y);
	platform->body = cpSpaceAddBody(&game.space, cpBodyNew(mass, moment));
	platform->shape = cpSpaceAddShape(&game.space, cpBoxShapeNew(platform->body, size.x, size.y, 1));
	cpBodySetPosition(platform->body, cpv(platform->bounds.x+size.x/2, platform->bounds.y+size.y/2));
	cpShapeSetCollisionType(platform->shape, COLLISION_TYPE_LEVEL);
	cpShapeSetUserData(platform->shape, (cpDataPointer)&platform->shapeUserData);
	platform->toMakeFalling = 0;
}

static void AddPickup(Level_t* lv, LV_Pickup_e type, Vector2 pos){
	LV_Pickup_t* pickup = NULL;
	int pickupId = -1;
	if(lv->pickupCount == LV_MAX_PICKUPS) return;
	if(lv->recycleLine > 0){
		for(int i=0; i < lv->pickupCount; i++){
			LV_Pickup_t* otherPickup = &lv->pickups[i];
			if(otherPickup->type && (otherPickup->initialPos.y < lv->recycleLine)){
				RemovePickup(i);
				pickupId = i;
				pickup = otherPickup;
				break;
			}
		}
	}
	if(pickup == NULL){
		pickupId = lv->pickupCount++;
		pickup = &lv->pickups[pickupId];
	}
	pickup->type = type;
	pickup->initialPos = pos;
	pickup->body = cpSpaceAddBody(&game.space, cpBodyNew(1, 0.5));
	pickup->shape = cpSpaceAddShape(&game.space, cpBoxShapeNew(pickup->body, 30, 30, 1));
	pickup->tform.size = (Vector2){40, 40};
	cpBodySetPosition(pickup->body, cpv(pos.x, pos.y));
	pickup->shapeUserData = (ShapeUserData_t){
		.type = SHAPE_LV_PICKUP,
		.id = pickupId
	};
	cpShapeSetCollisionType(pickup->shape, COLLISION_TYPE_LEVEL);
	cpShapeSetUserData(pickup->shape, (cpDataPointer)&pickup->shapeUserData);
	pickup->toBeRemoved = 0;
}

void RemovePickup(int id){
	Level_t* lv = &game.level;
	LV_Pickup_t* pickup = &lv->pickups[id];
	pickup->type = PICKUP_NONE;
	if(pickup->body){
		cpSpaceRemoveBody(&game.space, pickup->body);
		pickup->body = NULL;
	}
	if(pickup->shape){
		cpSpaceRemoveShape(&game.space, pickup->shape);
		pickup->shape = NULL;
	}
	if(id == (lv->pickupCount-1)){
		lv->pickupCount--;
	}
}

static void UpdateBounds(Level_t* lv){
	cpFloat boundsHeight = 10000;
	cpFloat radius = 50;
	for(int i=0; i < 2; i++){
		if(!lv->bounds[i]) continue;
		cpSpaceRemoveShape(&game.space, lv->bounds[i]);
		lv->bounds[i] = NULL;
	}
	lv->bounds[0] = cpSpaceAddShape(&game.space, cpSegmentShapeNew(cpSpaceGetStaticBody(&game.space), cpv(-lv->sideLava-radius, lv->generatedDepth-boundsHeight), cpv(-lv->sideLava-radius, lv->generatedDepth+boundsHeight), radius));
	lv->bounds[1] = cpSpaceAddShape(&game.space, cpSegmentShapeNew(cpSpaceGetStaticBody(&game.space), cpv(lv->sideLava+radius, lv->generatedDepth-boundsHeight), cpv(lv->sideLava+radius, lv->generatedDepth+boundsHeight), radius));
}

void LoadLevel(int param){
	Level_t* lv = &game.level;
	lv->generatedDepth = 0;
	lv->cursor = Vector2Zero();
	lv->recycleLine = -10000;
	
	lv->platformCount = 0;
	lv->pickupCount = 0;

	
	// AddPickup(lv, PICKUP_HEALTH, (Vector2){-250, -200});
	// AddPickup(lv, PICKUP_FALLING_PLATFORMS, (Vector2){-200, -200});

	lv->sideLava = 800;
	UpdateBounds(lv);
}

void ClearLevel(){
	Level_t* lv = &game.level;
	for(int i=0; i < lv->platformCount; i++){
		RemovePlatformData(i);
	}
	lv->platformCount = 0;
	for(int i=0; i < 2; i++){
		if(!lv->bounds[i]) continue;
		cpSpaceRemoveShape(&game.space, lv->bounds[i]);
		lv->bounds[i] = NULL;
	}
	for(int i=0; i < lv->pickupCount; i++){
		RemovePickup(i);
	}
	lv->pickupCount = 0;
}

static void Generate(Level_t* lv){
	float generationSize = 2000;
	Vector2 cursor = lv->cursor;
	lv->generatedDepth += generationSize;

	if(cursor.y < 100) cursor.y = 140;

	int platformWidth = 100;
	while(cursor.y < lv->generatedDepth){
		int difficulty = cursor.y / 1000;
		if(difficulty > 100) difficulty = 100;

		if(difficulty < 1){
			cursor.y += 200+difficulty;
			cursor.x = GetRandomValue(-300, 300);
			platformWidth = GetRandomValue(80,120);
			AddPlatform((Rectangle){
				-lv->sideLava, cursor.y, lv->sideLava+(cursor.x-platformWidth), 20
			});
			AddPlatform((Rectangle){
				cursor.x+platformWidth, cursor.y, lv->sideLava-platformWidth, 20
			});
			if(!(rand()&7)){
				AddPickup(lv, PICKUP_HEALTH, (Vector2){cursor.x + 20, cursor.y - 50});
			}else if(!(rand()&15)){
				AddPickup(lv, PICKUP_FALLING_PLATFORMS, (Vector2){cursor.x + 20, cursor.y - 50});
			}
		}else{
			cursor.y += GetRandomValue(180, 240+difficulty*3);
			cursor.x += (platformWidth/2+GetRandomValue(10, 120))*( (rand()&1)*2-1 );
			platformWidth = GetRandomValue(10, 25-(difficulty/20) )*50;
			if(cursor.x < -lv->sideLava) cursor.x = -lv->sideLava;
			if((cursor.x+platformWidth) > lv->sideLava) cursor.x = lv->sideLava-platformWidth;

			AddPlatform((Rectangle){
				cursor.x, cursor.y, platformWidth, 20
			});
			if(!(lv->platformCount&7)){
				AddPlatform((Rectangle){
					(rand()&1) ? (-lv->sideLava) : (lv->sideLava-60),
					cursor.y,
					60, 60
				});
			}
			
			if(!(rand()&15)){
				AddPickup(lv, PICKUP_HEALTH, (Vector2){cursor.x+platformWidth/2, cursor.y - 50});
			}else if(!(rand()&31)){
				AddPickup(lv, PICKUP_FALLING_PLATFORMS, (Vector2){cursor.x+platformWidth/2, cursor.y - 50});
			}
		}
	}

	lv->cursor = cursor;
	UpdateBounds(lv);
	//printf("Generated. %f. platforms: %i\n", lv->generatedDepth, lv->platformCount);
}

void UpdateLevel(){
	Level_t* lv = &game.level;
	float generationThreshold = 1000;
	float recycleThreshold = 5000;

	for(int i=0; i < lv->platformCount; i++){
		LV_Platform_t* platform = &lv->platforms[i];
		if(platform->toMakeFalling){
			MakePlatformFall(i);
			//continue;
		}
		if(platform->body){
			UpdateTransform(&platform->tform, platform->body);
		}
	}

	for(int i=0; i < lv->pickupCount; i++){
		LV_Pickup_t* pickup = &lv->pickups[i];
		if(!pickup->type) continue;

		if(pickup->toBeRemoved){
			RemovePickup(i);
			continue;
		}
		UpdateTransform(&pickup->tform, pickup->body);
	}

	forEachPlayer(player, playerId){
		if((!player->isBot) && (player->guyId != -1)){
			Guy_t* guy = GetGuy(player->guyId);
			if(!guy->parts[0].active) continue;
			Vector2 guyPos = PeekPos(&guy->parts[0].tform);
			float newRecycleLine = guyPos.y - recycleThreshold;

			if(guyPos.y > (lv->generatedDepth - generationThreshold)){
				Generate(lv);
			}
			if(newRecycleLine > lv->recycleLine){
				lv->recycleLine = newRecycleLine;
			}
		}
	}
}

void DrawLevelBG(){
	Level_t* lv = &game.level;

	Color lavaColor = {254, 157, 27, 255};
	Color lavaColorBorder = {223, 97, 36, 255};
	DrawRectangle(-lv->sideLava-client.width, client.cam.target.y-client.halfHeight, client.width, client.height, lavaColor);
	DrawRectangle(lv->sideLava, client.cam.target.y-client.halfHeight, client.width, client.height, lavaColor);
	DrawRectangle(-lv->sideLava-client.width-25, client.cam.target.y-client.halfHeight, 25, client.height, lavaColorBorder);
	DrawRectangle(lv->sideLava, client.cam.target.y-client.halfHeight, 25, client.height, lavaColorBorder);

	DrawText("WASD - move.\nSpace - restart\nK - suicide", 65, 0, 40, (Color){30,0,40, 140});
}

void DrawLevel(){
	Level_t* lv = &game.level;

	for(int i=0; i < lv->platformCount; i++){
		Color fillColor = {58,173,52,255};
		LV_Platform_t* platform = &lv->platforms[i];
		if(platform->body){
			Vector2 pos = GetPos(&platform->tform);
			Vector2 size = {platform->bounds.width, platform->bounds.height};
			Rectangle rec = {pos.x, pos.y, size.x, size.y};
			Vector2 origin = {size.x/2, size.y/2};
			float angle = GetAngle(&platform->tform);
			DrawRectanglePro(rec, origin, angle, DARKGREEN);
			rec.x += 2; rec.y += 2;
			rec.width -= 4; rec.height -= 4;
			DrawRectanglePro(rec, origin, angle, fillColor);
		}else{
			if(!isInView(&platform->tform)) continue;
			DrawRectangleRec(platform->bounds, fillColor);
			DrawRectangleLinesEx(platform->bounds, 2, DARKGREEN);
		}
	}

	for(int i=0; i < lv->pickupCount; i++){
		LV_Pickup_t* pickup = &lv->pickups[i];
		if(!pickup->type) continue;
		//if(!isInView(&pickup->tform)) continue;

		Vector2 pos = GetPos(&pickup->tform);
		Vector2 size = pickup->tform.size;
		Rectangle rec = {pos.x, pos.y, size.x, size.y};
		Vector2 origin = {size.x/2, size.y/2};
		float angle = GetAngle(&pickup->tform);

		switch(pickup->type){
			case PICKUP_HEALTH: {
				// DrawRectanglePro(rec, origin, angle, BLACK);
				// rec.width *= 0.95;
				// rec.height *= 0.95;
				// DrawRectanglePro(rec, origin, angle, WHITE);
				// DrawRectangle(pos.x-5, pos.y-15, 10, 30, RED);
				// DrawRectangle(pos.x-15, pos.y-5, 30, 10, RED);
				Color bottle1 = {22,177,27,100};
				Color bottle2 = {46,122,41,100};
				Color blik = {226,222,65,30};
				DrawRectangleGradientV(pos.x-15, pos.y-50, 30, 80, bottle1, bottle2);
				DrawRectangle(pos.x-6, pos.y-50-20, 14, 20, bottle1);
				DrawCircleGradient(pos.x,pos.y-35, 15, blik, (Color){0,0,0,0});
				DrawRectangle(pos.x-13, pos.y-25, 26, 25, LIGHTGRAY);
				DrawRectangle(pos.x-3, pos.y-10-8, 6, 16, RED);
				DrawRectangle(pos.x-8, pos.y-10-3, 16, 6, RED);

			} break;
			case PICKUP_FALLING_PLATFORMS:{
				// DrawRectanglePro(rec, origin, angle, DARKGREEN);
				// DrawCircle(pos.x, pos.y, 16, GREEN);
				// DrawCircle(pos.x, pos.y, 14, DARKGREEN);
				// DrawLineEx((Vector2){pos.x-10, pos.y-10}, (Vector2){pos.x+10,pos.y+10}, 6, GREEN);

				Color bottle1 = {192,73,33,100};
				Color bottle2 = {118,70,50,100};
				Color blik = {226,222,65,30};
				DrawRectangleGradientV(pos.x-15, pos.y-50, 30, 80, bottle1, bottle2);
				DrawRectangle(pos.x-8, pos.y-50-20, 16, 20, bottle1);
				DrawCircleGradient(pos.x,pos.y-35, 15, blik, (Color){0,0,0,0});
				DrawRectangle(pos.x-13, pos.y-25, 26, 25, LIGHTGRAY);

				DrawCircle(pos.x, pos.y-10, 10, GREEN);
				DrawCircle(pos.x, pos.y-10, 8, DARKGREEN);
				DrawLineEx((Vector2){pos.x-10, pos.y-10-10}, (Vector2){pos.x+10,pos.y-10+10}, 6, GREEN);

			} break;
		}
	}
}
