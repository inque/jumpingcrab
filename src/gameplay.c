#include "gameapp.h"

void UpdateGameplay(){
	switch(game.state){
		case STATE_UNINITIALIZED: {

			Vector2 spawnPos = {0, -200};
			forEachPlayer(player, playerId){
				player->guyId = SpawnGuy(GUY_DOG, playerId, spawnPos);
				AddPlatform((Rectangle){spawnPos.x-150, spawnPos.y+160, 300, 20});

				spawnPos.x += 360;
				if(spawnPos.x > game.level.sideLava){
					spawnPos.x = -game.level.sideLava + (spawnPos.x - game.level.sideLava);
					spawnPos.y += 200;
				}
			}
			game.state = STATE_STARTING;
			game.timer = 0;
		} break;
		case STATE_STARTING: {
			if(game.timer++ > 2*TICKS_PER_SEC){
				game.state = STATE_PLAYING;
				game.timer = 0;
			}
		} break;
		case STATE_PLAYING: {
			int peoplePlaying = 0;
			forEachPlayer(player, playerId){
				if((!player->isBot) && (player->guyId != -1)){
					Guy_t* guy = GetGuy(player->guyId);
					Vector2 pos = PeekPos(&guy->parts[0].tform);
					int newscore = (int)pos.y;
					if(newscore > player->score){
						player->score = newscore;
					}
					if(newscore > app.highScore){
						app.highScore = newscore;
						app.config.highScore = app.highScore;
					}
					peoplePlaying++;
				}
			}
			
			if(!peoplePlaying){
				game.state = STATE_ENDED;
				game.timer = 10*TICKS_PER_SEC;
			}
		} break;
		case STATE_ENDED: {
			if(!game.timer--){
				app.shouldRestartRound = 1;
			}
		} break;
	}
}
