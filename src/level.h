#pragma once
#include "raymath.h"
#include "physics.h"

#define LV_MAX_PLATFORMS 512
#define LV_MAX_PICKUPS 64

typedef struct{
	Rectangle bounds;
	cpShape* shape;
	cpBody* body;
	Transform_t tform;
	ShapeUserData_t shapeUserData;
	int toMakeFalling;
} LV_Platform_t;

typedef enum{
	PICKUP_NONE,
	PICKUP_HEALTH,
	PICKUP_FALLING_PLATFORMS
} LV_Pickup_e;

typedef struct{
	LV_Pickup_e type;
	Vector2 initialPos;
	cpBody* body;
	cpShape* shape;
	Transform_t tform;
	ShapeUserData_t shapeUserData;
	int toBeRemoved;
} LV_Pickup_t;

typedef struct{
	int platformCount;
	LV_Platform_t platforms[LV_MAX_PLATFORMS];
	int pickupCount;
	LV_Pickup_t pickups[LV_MAX_PICKUPS];
	cpShape* bounds[2];
	float sideLava;
	float generatedDepth;
	float recycleLine;
	Vector2 cursor;
} Level_t;

void LoadLevel(int param);
void ClearLevel();
void AddPlatform(Rectangle bounds);
void RemovePickup(int id);
void MakePlatformFall(int id);
void UpdateLevel();
void DrawLevelBG();
void DrawLevel();
