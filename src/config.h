#pragma once

typedef struct{
	int fullscreen;
	int vsync;
	int resolution[2];
	int windowPos[2];
	float soundVolume;
	float musicVolume;
	int tickrate;
	int interpolate;
	int port;
	float camWobbleAmplitude;
	float camWobbleSpeed;
	float motionBlur;
	int highScore;
} AppConfig_t;

void DefaultSettings();
void LoadSettings();
void SaveSettings();
