#pragma once
#include "raymath.h"

#define MAX_PARTICLES 1024

typedef enum{
    EFFECT_HEAL,
    EFFECT_BLOOD_SPLASH,
    EFFECT_DAMAGE_LOCAL
} ParticleEffect_e;

typedef enum{
    PARTICLE_NONE,
    PARTICLE_BONUS,
    PARTICLE_BLOOD
} ParticleType_e;

typedef struct{
    int type;
    float life;
    Vector2 pos;
    Vector2 velocity;
} Particle_t;

typedef struct{
    int particleCount, nextFreeParticle;
    Particle_t particles[MAX_PARTICLES];
} ParticleSystem_t;

void SpawnParticles(ParticleEffect_e effect, int amount, Vector2 pos, Vector2 velocity);
void DrawParticles();
