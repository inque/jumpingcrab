#ifndef DEDICATED_SERVER

#include "gameapp.h"
#include "raylib.h"
#include "math.h"

static struct{
    int isPlaying;
    int bar;
	int pattern;
	int seed;
	float master;
	int fadingOut;
} music;

void LoadAssets(){
	client.assets.musBeat[0] = LoadSound("data/beat1.ogg");
	client.assets.musBeat[1] = LoadSound("data/beat2.ogg");
	client.assets.musBeat[2] = LoadSound("data/beat3.ogg");
	client.assets.musBeat[3] = LoadSound("data/beat4.ogg");
	// client.assets.melody1 = LoadSound("data/melody1.ogg");
	// client.assets.bass1 = LoadSound("data/bass1.ogg");
	client.assets.melody1 = LoadMusicStream("data/melody1.ogg");
	client.assets.bass1 = LoadMusicStream("data/bass1.ogg");
	client.assets.sfxLose = LoadSound("data/sfx_lose.ogg");
	client.assets.sfxFall1 = LoadSound("data/fall1.ogg");
	music.seed = GetRandomValue(0, 1000);
}

void UpdateMusic(){
	float beatPhase[] = {
		0.5+0.5*fabsf(sinf(3.14*120*2*0.5*client.time)),
		0.5+0.5*fabsf(sinf(3.14*120*3*0.5*client.time)),
		0.5+0.5*fabsf(sinf(3.14*120*4*0.5*client.time)),
		0.5+0.5*fabsf(sinf(3.14*120*6*0.5*client.time))
	};
	float beatVol[] = {
		0.9,
		0.0,
		0.5,
		0.0
	};

	if(music.isPlaying){
		
		for(int i=0; i < MUS_BEATS; i++){
			if(!IsSoundPlaying(client.assets.musBeat[i])){
				float amp = beatVol[ (i+music.pattern+music.seed*3) % MUS_BEATS ];
				amp *= beatPhase[ (2*(3+i+music.pattern+music.seed)) % MUS_BEATS ];
				amp *= music.master;
				if( (i != 0) && ( !((i*5-music.pattern+music.seed*5)&15) ) ){
					SetSoundPitch(client.assets.musBeat[i], 0.5);
				}else{
					SetSoundPitch(client.assets.musBeat[i], 1);
				}
				SetSoundVolume(client.assets.musBeat[i], amp);
				PlaySound(client.assets.musBeat[i]);

				if(i==0){
					if(music.bar > 8){
						music.bar = 0;
						music.pattern++;
					}else{
						music.bar++;
					}
				}
			}
		}

		// if(!IsSoundPlaying(client.assets.melody1)){
		// 	if(music.pattern&1){
		// 		SetSoundPitch(client.assets.melody1, 0.25);
		// 		PlaySound(client.assets.melody1);
		// 	}
		// }
		// if(!IsSoundPlaying(client.assets.bass1)){
		// 	//if(music.pattern&1){
		// 		//SetSoundPitch(client.assets.bass1, 0.25);
		// 		PlaySound(client.assets.bass1);
		// 	//}
		// }
		// SetSoundVolume(client.assets.melody1, 0.6*music.master);
		// SetSoundVolume(client.assets.bass1, 0.5*music.master);

		if(!IsMusicPlaying(client.assets.melody1)){
			if(music.pattern&1){
				SetMusicPitch(client.assets.melody1, 0.25);
				PlayMusicStream(client.assets.melody1);
			}
		}
		if(!IsMusicPlaying(client.assets.bass1)){
			if((music.pattern&7) != 7){
				//SetSoundPitch(client.assets.bass1, 0.25);
				PlayMusicStream(client.assets.bass1);
			}
		}
		SetMusicVolume(client.assets.melody1, 0.6*music.master);
		SetMusicVolume(client.assets.bass1, 0.5*music.master);
		UpdateMusicStream(client.assets.melody1);
		UpdateMusicStream(client.assets.bass1);

		if(music.fadingOut){
			if(music.master > 0){
				music.master -= 0.05;
			}else{
				music.isPlaying = 0;
				music.master = 0;
			}
		}else{
			if(music.master < 1){
				music.master += 0.05;
			}
			if(client.screen != SCREEN_GAME){
				music.fadingOut = 1;
			}
		}
		if(music.master > app.config.musicVolume){
			music.master = app.config.musicVolume;
		}

	}else{
		if(client.screen == SCREEN_GAME){
			music.isPlaying = 1;
			music.bar = 0;
			music.pattern = 0;
			music.master = 0;
			music.fadingOut = 0;
		}
	}
}

#endif

void PlaySFX(SfxType_e sfx, Vector2 pos, float vol, int playerId){
	#ifndef DEDICATED_SERVER
		if((playerId != -1) && (playerId != client.localPlayerId)) return;
		Sound snd = client.assets.sfxFall1;
		int multi = 1;
		switch(sfx){
			case SFX_LOSE:
				snd = client.assets.sfxLose;
				break;
			case SFX_FALL:
				snd = client.assets.sfxFall1;
				multi = 24;
				break;
		}
		float maxDist = 1500;
		float dist = fmaxf(1-Vector2Distance(client.cam.target, pos)/maxDist, 0);
		//float pan = (client.cam.target.x - pos.x)/maxDist;
		float total = vol*dist;
		if(total > 1) total = 1;
		if(total < 0) total = 0;
		SetSoundVolume(snd, total*app.config.soundVolume);
		SetSoundPitch(snd, GetRandomValue(70, 120)/100.0 );
		//idk how to pan
		if(multi < 2){
			PlaySound(snd);
		}else{
			if(GetSoundsPlaying() < multi){
				PlaySoundMulti(snd);
			}
		}
	#endif
}
