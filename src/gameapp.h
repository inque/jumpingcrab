#pragma once
#include "players.h"
#include "guys.h"
#include "level.h"
#include "physics.h"
#include "transforms.h"
#include "particles.h"
#include "config.h"
#include "assets.h"
#include "raylib.h"

#define GAME_TITLE "Crabster go home"
#define TICKS_PER_SEC 60
#define GET_GAME_TICKRATE() app.config.tickrate

#define MAX_PLAYERS 16
#define MAX_GUYS 16

typedef enum{
	STATE_UNINITIALIZED,
	STATE_STARTING,
	STATE_PLAYING,
	STATE_ENDED
} GameState_e;

typedef struct{
	int initialized;
	int frame;
	int timer;
	GameState_e state;
	Player_t players[MAX_PLAYERS];
	Guy_t guys[MAX_GUYS];
	Level_t level;
	cpSpace space;
} GameState_t;

typedef struct{
	char configPath[64];
	AppConfig_t config;
	struct{
		int addFeet;
		float bodyWidth;
	} secretOptions;
	int shouldRestartRound;
	int gamesPlayed;
	int highScore;
} AppState_t;

extern AppState_t app;
extern GameState_t game;

#ifndef DEDICATED_SERVER

typedef enum{
	SCREEN_MAINMENU,
	SCREEN_GAME
} ClientScreen_e;

typedef struct{
	ClientScreen_e screen;
	int width, height;
	int halfWidth, halfHeight;
	float dpi, scale;
	float time, deltaTime;
	float lastTickTime, deltaFrame;
	Camera2D cam;
	Rectangle viewBounds;
	int shouldClose;
	int localPlayerId;
	ParticleSystem_t fx;
	int paused;
	float camWobblePhase;
	float camWobblePower;
	ClientAssets_t assets;
	RenderTexture2D fsQuad;
} ClientState_t;

extern ClientState_t client;

#endif

static inline Player_t* GetPlayer(int id){
	return &game.players[id];
}

static inline Guy_t* GetGuy(int id){
	return &game.guys[id];
}

#define forEach(T, Get, Max, Handle, Id) \
			int Id = 0; \
			for(T* Handle = Get(0); Id < Max; Handle = Get(++Id)) \
			    if(Handle->active)

#define forEachPlayer(Handle, Id) \
			forEach(Player_t, GetPlayer, MAX_PLAYERS, Handle, Id)

#define forEachGuy(Handle, Id) \
			forEach(Guy_t, GetGuy, MAX_GUYS, Handle, Id)

void GameTick();
void DrawGraphics();
void StartGame();
void ResetGame();
void CloseGame(bool full);

#ifdef _MSC_VER
	//disable float conversion warnings
	#pragma warning(disable:4244 4305)
#endif
