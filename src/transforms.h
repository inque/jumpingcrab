#pragma once
#include "raymath.h"
#include "physics.h"

#define Vec2(v) (Vector2){v.x, v.y}

typedef struct{
    Vector2 pos[2]; //2-tap interpolation
    float angle[2];

    Vector2 size;
} Transform_t;

void PinTransform(Transform_t* tform, Vector2 pos, float angle, Vector2 size);
void UpdateTransform(Transform_t* tform, cpBody* body);
Vector2 PeekPos(Transform_t* tform); //no interp
Vector2 GetPos(Transform_t* tform); //interp
float PeekAngle(Transform_t* tform); //radians
float GetAngle(Transform_t* tform); //degrees

Rectangle GetBounds(Transform_t* tform);
bool DoesIntersectRect(Transform_t* tform, Rectangle rec);
bool isInView(Transform_t* tform);
