# About

A game from Ludum Dare 48 jam. (4/24/2021)

You can play it here: https://nikky.dev/games/crab/

Controls: WASD

# Dev log

```
hour 1. set up project and physics
hour 2. inputs, physics, transforms
hour 3. camera, level generation
hour 4. level cycling, fall damage
hour 5. added legs
hour 6. added (terrible) controls
hour 7. physics, UI
hour 8. messing with physics
hour 9. level bounds
hour 10. health pickups
hour 11. particle effects
hour 12. falling platforms
hour 13. controls
hour 14. minor improvements
hour 15. web build
hour 16. pause menu, feet
hour 17. attempted multiplayer, gave up
decided not to upload
-- changed my mind 1 day later --
hour 18. graphics
hour 19. music
hour 20. music
hour 21. sfx
hour 22. fixing web build
```
